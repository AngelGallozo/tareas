<?php
/*
 * This file belongs to the YITH PB Plugin Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PB_Shortcodes' ) ) {

	class YITH_PB_Shortcodes {

        /**
		 * Main Instance
		 *
		 * @var YITH_PB_Shortcodes
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		
        /**
         * Main plugin Instance
         *
         * @return YITH_PB_Post_Types Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PB_Post_Types constructor.
		 */
		private function __construct() {

			$shortcodes = array(
				'yith_pb_show_post_type'       => __CLASS__ . '::show_post_types', // print book.
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}

		}

		//Display books and their content
        public static function show_post_types( $atts ) {            
            
			$args = array(
                            'post_type'      => 'pb-book',
                            'posts_per_page' => get_option( 'yith_pb_shortcode_number', 5 ),
                            'publish_status' => 'published',
                         );
         
            $query = new WP_Query( $args );
            
            ob_start();
         
            if( $query->have_posts() ) :
         
                while( $query->have_posts() ) :
         
                    $query->the_post();
                    
                    echo '<p><strong>' . esc_html__( 'Title: ', 'yith-plugin-book' ) .  '</strong> <h1>' . get_the_title() . '</h1></p>';
                    echo '<p><strong>' . esc_html__( 'Description: ', 'yith-plugin-book' ) .  '</strong>' . get_the_content() . '</p>';
                    
                    if( get_option( 'yith_pb_shortcode_show_image' ) ){
                        echo '<p><strong>' . esc_html__( 'Cover: ', 'yith-plugin-book' )  .  '</strong>'  . get_the_post_thumbnail() . '</p>';    
                    }

                    echo '<p><strong>' . esc_html__( 'ISBN: ', 'yith-plugin-book' ) . '</strong>' . get_post_meta( get_the_ID(), '_yith_pb_isbn', true ) . '</p>';
                    echo '<p><strong>' . esc_html__( 'Price: ', 'yith-plugin-book' ) . '</strong>' . get_post_meta( get_the_ID(), '_yith_pb_price', true ) . ' $</p>';
                    echo '<p><strong>' . esc_html__( 'Cap Type: ', 'yith-plugin-book' ) . '</strong>' . get_post_meta( get_the_ID(), '_yith_pb_cap-type', true ) . '</p>';
                    echo '<p><strong>' . esc_html__( 'Language: ', 'yith-plugin-book' ) . '</strong>' . get_post_meta( get_the_ID(), '_yith_pb_language', true ) . '</p>';
            	endwhile;
         		wp_reset_postdata();
         
            endif;
            $value = ob_get_clean();
            return $value;
        }
	}	
}