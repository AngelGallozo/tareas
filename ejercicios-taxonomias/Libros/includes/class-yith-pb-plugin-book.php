<?php
/*
 * This file belongs to the YITH PB Plugin Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PB_Plugin_Skeleton' ) ) {
	class YITH_PB_Plugin_Book {

        /**
		 * Main Instance
		 *
		 * @var YITH_PB_Plugin_Book
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
        /**
		 * Main Admin Instance
		 *
		 * @var YITH_Pb_Plugin_Book_Admin
		 * @since 1.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_PB_Plugin_Book_Frontend
		 * @since 1.0
		 */
        public $frontend = null;

		/**
		 * Main Shortcodes Instance
		 * @var YITH_PB_Plugin_Book_Frontend
		 * @since 1.0
		 */
        public $shortcodes = null;

        /**
         * Main plugin Instance
         *
         * @return YITH_PB_Plugin_Book Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
			// ternary operator --> https://www.codementor.io/@sayantinideb/ternary-operator-in-php-how-to-use-the-php-ternary-operator-x0ubd3po6 
        }
        
		/**
		 * YITH_PB_Plugin_Book constructor.
		 */
		private function __construct() {
            $require = apply_filters('yith_pb_require_class',
				 array(
					'common'   => array(
						'includes/class-yith-pb-post-types.php',
						'includes/functions.php',
						'includes/class-yith-pb-shortcodes.php'
					),
					'admin' => array(
						'includes/class-yith-pb-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-pb-frontend.php',
					),
				)
			);

			$this->_require($require);
			$this->init_classes();
			/* 
				Here set any other hooks ( actions or filters you'll use on this class)
			*/
			
			// Finally call the init function
			$this->init();
		}
	
		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param $main_classes array The require classes file path
		 *
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require($main_classes)
		{
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && !is_admin() || (defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_PB_DIR_PATH . $class ) ) {
						require_once( YITH_PB_DIR_PATH . $class );
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 * @since  1.0
		 **/
		public function init_classes(){
			$this->shortcodes = YITH_PB_Shortcodes::get_instance();
			YITH_PB_Post_Types::get_instance();
		}

		/**
         * Function init()
         *
         * Instance the admin or frontend classes
         *
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         * @since  1.0
         * @return void
         * @access protected
         */
        public function init()
        {
            if (is_admin()) {
                $this->admin =  YITH_PB_Admin::get_instance();
            }

            if (!is_admin() || (defined('DOING_AJAX') && DOING_AJAX)) {
                $this->frontend = YITH_PB_Frontend::get_instance();
            }
        }

	}	
}
/**
 * Get the YITH_PB_Plugin_Book instance
 *
 * @return YITH_PB_Plugin_Book
 */
if ( ! function_exists( 'yith_pb_plugin_book' ) ) {
	function yith_pb_plugin_book() {
		return YITH_PB_Plugin_Book::get_instance();
	}
}