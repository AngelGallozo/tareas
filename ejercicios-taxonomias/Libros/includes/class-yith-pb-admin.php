<?php
/*
 * This file belongs to the YITH PB Plugin Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PB_Admin' ) ) {

	class YITH_PB_Admin {

        /**
		 * Main Instance
		 *
		 * @var YITH_PB_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
         * @return YITH_PB_Admin Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }

		/**
		 * YITH_PB_Admin constructor.
		 */
		private function __construct() {
			add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'pb_ag_metabox_save') );

			// Add columns to the book wp_list_tables
			add_filter( 'manage_pb-book_posts_columns', array( $this, 'add_book_post_type_columns' ) );

			// display data in each post_type_column
			add_action('manage_pb-book_posts_custom_column', array( $this, 'display_book_post_type_custom_column'), 10, 2 );


			// Admin menu

			add_action('admin_menu', array( $this, 'create_menu_for_general_options') );
			add_action( 'admin_init', array( $this, 'register_settings' ) );
		}

		/**
		 * Setup the meta boxes
		 */
		public function add_meta_boxes() {
			add_meta_box( 'yith-ps-additional-information', __( 'Additional information', 'yith-plugin-pb' ), array(
			$this, 'view_meta_boxes' ), YITH_PB_Post_Types::$post_type );
		}

		/**
		 * View meta boxes
		 *
		 * @param $post
		 */
		public function view_meta_boxes( $post ) {
			yith_pb_get_view( '/metaboxes/plugin-book-info-metabox.php', array( 'post' => $post) );
		}

		/**
		 * Save metabox values
		 * @param $post_id
		 */
		public function pb_ag_metabox_save( $post_id ) {
			// validate if user can edit the post
			if ( ! current_user_can( 'edit_post' ) ) {
				return;
			}

			if ( YITH_PB_Post_Types::$post_type !== get_post_type( $post_id ) ) {
				return;
			}

			if ( isset( $_POST[ '_yith_pb_isbn' ] ) ) {
				update_post_meta( $post_id, '_yith_pb_isbn', $_POST[ '_yith_pb_isbn' ] );
			}	

			if ( isset( $_POST[ '_yith_pb_price' ] ) ) {
				update_post_meta( $post_id, '_yith_pb_price', $_POST[ '_yith_pb_price' ] );
			}
			if ( isset( $_POST[ '_yith_pb_cap-type' ] ) ) {
				update_post_meta( $post_id, '_yith_pb_cap-type', $_POST[ '_yith_pb_cap-type' ] );
			}
			if ( isset( $_POST[ '_yith_pb_language' ] ) ) {
				update_post_meta( $post_id, '_yith_pb_language', $_POST[ '_yith_pb_language' ] );
			}
		}


		/**
		 * Filters the columns displayed in the Posts list table for plugin Book post type.
		 *
		 * @param string[] $post_columns An associative array of column headings.
		 */
		public function add_book_post_type_columns( $post_columns ) {

			$new_columns = apply_filters( 'yith_pb_book_custom_columns ', array(
				'ISBN' => esc_html__( 'ISBN', 'yith-plugin-book' ),
				'Price' => esc_html__( 'Price', 'yith-plugin-book' ),
				'Cap_Type' => esc_html__( 'Cap_Type', 'yith-plugin-book' ),
				'Language' => esc_html__( 'Language', 'yith-plugin-book' ),
			) );

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;
		}

		/**
		 * Fires for each custom column of a specific post type in the Posts list table.
		 *
		 * @param string $column_name The name of the column to display.
		 * @param int    $post_id     The current post ID.
		 * */
		public function display_book_post_type_custom_column( $column_name, $post_id ) {

			switch ( $column_name ) {

				case 'ISBN':
						echo esc_html( get_post_meta( $post_id, '_yith_pb_isbn', true ) );
					break;
				case 'Price':
						echo esc_html( get_post_meta( $post_id, '_yith_pb_price', true ) );
					break;

				case 'Cap_Type':
						echo esc_html( get_post_meta( $post_id, '_yith_pb_cap-type', true ) );
					break;

				case 'Language':	
						echo esc_html( get_post_meta( $post_id, '_yith_pb_language', true ) );
					break;

				default  : do_action( 'yith_pb_book_display_custom_column', $column_name, $post_id);
					break;
			}

		}

		/**
		 *  Create menu for general options
		 */
		public function create_menu_for_general_options() {
				add_menu_page( 
					esc_html__( 'Plugin Book Options', 'yith-plugin-book' ),
					esc_html__( 'Plugin Book Options', 'yith-plugin-book' ),
					'manage_options',
					'plugin_book_options',
					array( $this, 'book_custom_menu_page' ),
					'',
					40
				); 

		}
		/**
		 *  Callback custom menu page
		 */
		function book_custom_menu_page() {
			yith_pb_get_view('/admin/plugin-options-panel.php', array() );
		}

		/**
		 * Add the fields in the shortcode attribute management page
		 */

		public function register_settings() {
			
			$page_name    = 'pb-options-page';
			$section_name = 'options_section';

			$setting_fields = array(
				array(
					'id'       => 'yith_pb_shortcode_number',
					'title'    => esc_html__( 'Number of Books', 'yith-plugin-book' ),
					'callback' => 'print_number_input'
				),
				array(
					'id'       => 'yith_pb_shortcode_show_image',
					'title'    => esc_html__( 'Show image', 'yith-plugin-book' ),
					'callback' => 'print_show_image'
				),
			);

			add_settings_section(
				$section_name,
				esc_html__( 'Section', 'yith-plugin-book' ),
				'',
				$page_name
			);

			foreach ( $setting_fields as $field ) {
				extract( $field );

				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);

				register_setting( $page_name, $id );
			}


		}

		/**
		 * Print the number input field
		 */
		public function print_number_input() {
			$tst_number = intval( get_option( 'yith_pb_shortcode_number', 5 ) );
			?>
            <input type="number" id="yith_pb_shortcode_number" name="yith_pb_shortcode_number"
                   value="<?php echo '' !== $tst_number ? $tst_number : 5; ?>">
			<?php
		}

		/**
		 * Print the show image toggle field
		 */
		public function print_show_image() {
			?>
            <input type="checkbox" class="pb-tst-option-panel__onoff__input" name="yith_pb_shortcode_show_image"
                   value='yes'
                   id="yith_pb_shortcode_show_image"
				<?php checked( get_option( 'yith_pb_shortcode_show_image', '' ), 'yes' ); ?>
            >
            <label for="shortcode_show_image" class="pb-tst-option-panel__onoff__label ">
                <span class="pb-tst-option-panel__onoff__btn"></span>
            </label>
			<?php
		}

	}	
}