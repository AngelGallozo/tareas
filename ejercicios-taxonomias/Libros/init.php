<?php

/*
 * Plugin Name: YITH Plugin Book
 * Description: Book for YITH Plugins
 * Version: 1.0.0
 * Author: Angel Gallozo
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-book
 */

! defined( 'ABSPATH' ) && exit;  // check if defined ABSPATH

/* where defined PATH for Style, Assets, Templates, Views */
if ( ! defined( 'YITH_PB_VERSION' ) ) {
	define( 'YITH_PB_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PB_DIR_URL' ) ) {
	define( 'YITH_PB_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PB_DIR_PATH' ) ) {
	define( 'YITH_PB_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PB_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PB_DIR_INCLUDES_PATH', YITH_PB_DIR_PATH . '/includes' );
}


if ( ! defined( 'YITH_PB_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PB_DIR_VIEWS_PATH', YITH_PB_DIR_PATH . 'views' );
}

// Different way to declare a constant.

! defined( 'YITH_PB_INIT' )               && define( 'YITH_PB_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_PB_SLUG' )               && define( 'YITH_PB_SLUG', 'yith-plugin-book' );
! defined( 'YITH_PB_SECRETKEY' )          && define( 'YITH_PB_SECRETKEY', 'zd9egFgFdF1D8Azh2ifA' );


if ( ! function_exists( 'yith_pb_init_classes' ) ) {
	/**
	 * Include the scripts
	 *
	 * @return void
	 */
	function yith_pb_init_classes() {

		load_plugin_textdomain( 'yith-plugin-book', false, basename( dirname( __FILE__ ) ) . '/languages/' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_PB_DIR_INCLUDES_PATH . '/class-yith-pb-plugin-book.php';

		if ( class_exists( 'YITH_PB_Plugin_Book' ) ) {
			/*
			*	Call the main function
			*/
			yith_pb_plugin_book();
		}
	}
}

add_action( 'plugins_loaded', 'yith_pb_init_classes', 11 );
