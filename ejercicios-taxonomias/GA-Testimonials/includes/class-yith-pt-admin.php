<?php
/*
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Admin' ) ) {
		
	/**
	 * YITH_PT_Admin
	 */
	class YITH_PT_Admin {
     	/**
	      * Main Instance
	      * @var YITH_PT_Admin
	      * @since 1.3.0
	      * @access private
	     */
		private static $instance;
		/**
		 * Main plugin Instance
         * @return YITH_PT_Admin Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }

		/**
		 * YITH_PT_Admin constructor.
		 */
		private function __construct() {
			add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'pt_ag_metabox_save') );
	
			// See definition on class-wp-post-list-table.php 
			add_filter( 'manage_pt-testimonial_posts_columns', array( $this, 'add_testimonial_post_type_columns' ) );
			// display data in each post_type_column
			add_action( 'manage_pt-testimonial_posts_custom_column', array( $this, 'display_testimonial_post_type_custom_column'), 10, 2 );

			// Add js for the shortcode.
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts') );

			
			// Admin menu

			add_action( 'admin_menu', array( $this, 'create_menu_for_general_options') );
			add_action( 'admin_init', array( $this, 'register_settings' ) );
		}

		/**
		 * Setup the meta boxes
		 */
		public function add_meta_boxes() {
			add_meta_box( 'yith-pt-additional-information', __( 'Additional information', 'yith-plugin-pt' ), array(
			$this, 'view_meta_boxes' ), YITH_PT_Post_Types::$post_type );
		}


		
		/**
		 * Enqueue_scripts
		 *
		 * @return void
		 */
		public function enqueue_scripts() {
			wp_enqueue_script( 'yith-pt-admin-shortcode-js', YITH_PT_DIR_ASSETS_JS_URL . '/shortcode.js', array( 'jquery' ), YITH_PT_VERSION, true );
			wp_enqueue_style( 'wp-color-picker' );			
    		wp_enqueue_script( 'my-script-handle', YITH_PT_DIR_ASSETS_JS_URL . '/shortcode.js', array( 'wp-color-picker' ), YITH_PT_VERSION, true );
		}

		/**
		 * View meta boxes
		 *
		 * @param $post
		 */
		public function view_meta_boxes( $post ) {
			yith_pt_get_view( '/metaboxes/plugin-testimonial-info-metabox.php', array( 'post' => $post) );
		}

		/**
		 * Save metabox values
		 * @param $post_id
		 */
		public function pt_ag_metabox_save( $post_id ) {
			// validate if user can edit the post.
			if ( ! current_user_can( 'edit_post' ) ) {
				return;
			}

			if ( YITH_PT_Post_Types::$post_type !== get_post_type( $post_id ) ) {
				return;
			}

			if ( isset( $_POST[ '_yith_pt_role' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_role', $_POST[ '_yith_pt_role' ] );
			}	

			if ( isset( $_POST[ '_yith_pt_company' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_company', $_POST[ '_yith_pt_company' ] );
			}
			
			if ( isset( $_POST[ '_yith_pt_email' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_email', $_POST[ '_yith_pt_email' ] );
			}

			if ( isset( $_POST[ '_yith_pt_web-site' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_web-site', $_POST[ '_yith_pt_web-site' ] );
			}
			if ( isset( $_POST[ '_yith_pt_rating' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_rating', $_POST[ '_yith_pt_rating' ] );
			}

			if ( isset( $_POST[ '_yith_pt_vip' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_vip', $_POST[ '_yith_pt_vip' ] );
			}

			if ( isset( $_POST[ '_yith_pt_badge' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_badge', $_POST[ '_yith_pt_badge' ] );
			}

			if( $_POST['_yith_pt_badge'] =='yes' ){
				if ( isset( $_POST[ '_yith_pt_badge-text' ] ) ) {
					update_post_meta( $post_id, '_yith_pt_badge-text', $_POST[ '_yith_pt_badge-text' ] );
				}

				if ( isset( $_POST[ '_yith_pt_badge-color' ] ) ) {
					update_post_meta( $post_id, '_yith_pt_badge-color', $_POST[ '_yith_pt_badge-color' ] );
				}
			}
		}

		/**
		 * Filters the columns displayed in the Testimonial list table for plugin testimonial post type.
		 *
		 * @param string[] $post_columns An associative array of column headings.
		 */
		public function add_testimonial_post_type_columns( $post_columns ) {

			$new_columns = apply_filters( 'yith_pt_testimonial_custom_columns ', array(
				'Role' => esc_html__( 'Role', 'yith-plugin-testimonials' ),
				'Company' => esc_html__( 'Company', 'yith-plugin-testimonials' ),
				'Email' => esc_html__( 'Email', 'yith-plugin-testimonials' ),
				'Stars' => esc_html__( 'Stars', 'yith-plugin-testimonials' ),
				'Vip' => esc_html__( 'Vip', 'yith-plugin-testimonials' ),
			) );

			$post_columns = array_merge( $post_columns, $new_columns );	
			return $post_columns;
		}

		/**
		 * Fires for each custom column of a specific post type in the Testimonial list table.
		 *
		 * @param string $column_name The name of the column to display.
		 * @param int    $post_id     The current post ID.
		 * */
		public function display_testimonial_post_type_custom_column( $column_name, $post_id ) {

			switch ( $column_name ) {

				case 'Role':
						echo esc_html( get_post_meta( $post_id, '_yith_pt_role', true ) );
					break;
				case 'Company':
						echo esc_html( get_post_meta( $post_id, '_yith_pt_company', true ) . ' : ' . get_post_meta( $post_id, '_yith_pt_web-site', true ) );
					break;

				case 'Email':
						echo esc_html( get_post_meta( $post_id, '_yith_pt_email', true ) );
					break;

				case 'Stars':	
						echo esc_html( get_post_meta( $post_id, '_yith_pt_rating', true ) );
					break;

					case 'Vip':	
						$is_vip = get_post_meta( $post_id, '_yith_pt_vip', true );
						if( $is_vip=='' || $is_vip=='no') {
							echo _e( 'No', 'yith-plugin-testimonials' );
						}
						if( $is_vip=='yes') { 
							echo _e( 'Yes', 'yith-plugin-testimonials' );
						}
					break;

				default  : do_action( 'yith_pt_testimonial_display_custom_column', $column_name, $post_id);
					break;
			}

		}


		/**
		 *  Create menu for general options
		 */
		public function create_menu_for_general_options() {
			add_menu_page( 
				esc_html__( 'Plugin Testimonial Options', 'yith-plugin-testimonials' ),
				esc_html__( 'Plugin Testimonial Options', 'yith-plugin-testimonials' ),
				'manage_options',
				'plugin_testimonial_options',
				array( $this, 'testimonial_custom_menu_page' ),
				'',
				40
			); 

		}
		/**
		 *  Callback custom menu page
		 */
		public function testimonial_custom_menu_page() {
			yith_pt_get_view('/admin/plugin-options-panel.php', array() );
		}

		/**
		 * Add the fields in the shortcode attribute management page
		 */

		public function register_settings() {
			$page_name    = 'pt-options-page';
			$section_name = 'options_section';

			$setting_fields = array(
				array(
					'id'       => '_yith_pt_def_show_number',
					'title'    => esc_html__( 'Number of Testimonials: ', 'yith-plugin-testimonials' ),
					'callback' => 'print_number_input'
				),
				array(
					'id'       => '_yith_pt_def_show_image',
					'title'    => esc_html__( 'Show image: ', 'yith-plugin-testimonials' ),
					'callback' => 'print_show_image'
				),
				array(
					'id'       => '_yith_pt_def_hover_effect',
					'title'    => esc_html__( 'Effect Hover: ', 'yith-plugin-testimonials' ),
					'callback' => 'print_select_hover'
				),

				array(
					'id'       => '_yith_pt_def_border_radius',
					'title'    => esc_html__( 'Border Radius: ', 'yith-plugin-testimonials' ),
					'callback' => 'print_pixel_border'
				),

				array(
					'id'       => '_yith_pt_def_links_color',
					'title'    => esc_html__( 'Links Color: ', 'yith-plugin-testimonials' ),
					'callback' => 'print_select_links_color'
				),

			);

			add_settings_section(
				$section_name,
				esc_html__( 'Configuration Options', 'yith-plugin-testimonials' ),
				'',
				$page_name
			);

			foreach ( $setting_fields as $field ) {
				extract( $field );

				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);

				register_setting( $page_name, $id );
			}

		}

		/**
		 * Print the number input field
		 */
		public function print_number_input() {
			$tst_number = intval( get_option( '_yith_pt_def_show_number', 6 ) );
			?>
			<input type="number" id ="_yith_pt_def_show_number" name="_yith_pt_def_show_number"
				value="<?php echo '' !== $tst_number ? esc_html( $tst_number ) : 6; ?>">
			<?php
		}

		/**
		 * Print the show image toggle field
		 */
		public function print_show_image() {
			?>
			<input type="checkbox" class="pt-tst-option-panel__onoff__input" name="_yith_pt_def_show_image"
				value='yes'
				id="_yith_pt_def_show_image"
				<?php checked( get_option( '_yith_pt_def_show_image', 'no' ), 'yes' ); ?>
			>
			<label for="def_show_image" class="pt-tst-option-panel__onoff__label ">
				<span class="pt-tst-option-panel__onoff__btn"></span>
			</label>
			<?php
		}

		/**
		 * Print hover effects options
		 */
		public function print_select_hover() {
			?>
			<select name="_yith_pt_def_hover_effect" id="_yith_pt_def_hover_effect">
				<option value="none" <?php if( get_option( '_yith_pt_def_hover_effect' ) == "none") echo " selected" ?>><?php  _e( 'None', 'yith-plugin-testimonials' ) ?> </option >
				<option value="zoom" <?php if( get_option( '_yith_pt_def_hover_effect' ) == "zoom") echo " selected" ?>><?php  _e( 'Zoom', 'yith-plugin-testimonials' ) ?> </option >
				<option value="highlight" <?php if( get_option( '_yith_pt_def_hover_effect' ) == "highlight") echo " selected" ?>><?php  _e( 'Highlight', 'yith-plugin-testimonials' ) ?> </option >
			</select>
			<?php
		}
		/**
		 * Print the number number pixel flied
		 */
		public function print_pixel_border() {
			$tst_number = intval( get_option( '_yith_pt_def_border_radius', 7 ) );
			?>
			<input type="number" id ="_yith_pt_def_border_radius" name="_yith_pt_def_border_radius"
				value="<?php echo '' !== $tst_number ? $tst_number : 7; ?>">
			<?php
		}

		/**
		 * Print the color links field
		 */
		public function print_select_links_color() {
			$tst_color = get_option( '_yith_pt_def_links_color', '#0073aa' );
		?>
			<label id='lb_links_color' for="tst_links_color"></label>
    		<input type="text" name="_yith_pt_def_links_color"  class="badge_color" value="<?php echo esc_html( get_option( '_yith_pt_def_links_color', '#0073aa' ) ); ?>" data-default-color="#0073aa" />    
			<?php
		}
	}

}