<?php
/**
 * This file belongs to the YITH PB Plugin Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PB_Post_Types' ) ) {
	/**
	 * YITH_PB_Post_Types
	 */
	class YITH_PB_Post_Types {
		/**
		 * Main Instance
		 *
		 * @var YITH_PB_Post_Types
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Post type name
		 *
		 * @var YITH_PB_Post_Types
		 * @since 1.0
		 * @access public
		 */
		public static $post_type = 'pb-book';
		/**
		 * Main plugin Instance
		 * @return YITH_PB_Post_Types Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PB_Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_type' ) );
			add_action( 'init', array( $this, 'register_taxonomy' ) );
		}
		/**
		 * Setup the 'Book' custom post type
		 */
		public function setup_post_type() {
			$args = array(
				'label'        => __( 'Book', 'yith-plugin-book' ),
				'description'  =>  __( 'Book post type', 'yith-plugin-book'),
				'public'       => true,
				'menu_icon'    => 'dashicons-universal-access',
				'show_in_menu' => true,
				'show_ui'      => true,
				'rewrite'      => false,
				'supports'     => array( 'title', 'editor', 'author', 'thumbnail' ),
			);
			register_post_type( self::$post_type, $args );
		}
		/**
		 * Registering taxonomy Author
		 *
		 * @return void
		 */
		public function register_taxonomy() {
			// Add Author taxonomy.
			$labels1 = array(
				'name'              => _x( 'Authors', 'taxonomy authors', 'yith-plugin-book' ),
				'singular_name'     => _x( 'Author', 'taxonomy authors', 'yith-plugin-book' ),
				'search_items'      => __( 'Search Authors', 'yith-plugin-book' ),
				'all_items'         => __( 'All Authors', 'yith-plugin-book' ),
				'parent_item'       => __( 'Parent Authors', 'yith-plugin-book' ),
				'parent_item_colon' => __( 'Parent Authors:', 'yith-plugin-book' ),
				'edit_item'         => __( 'Edit Author', 'yith-plugin-book' ),
				'update_item'       => __( 'Update Author', 'yith-plugin-book' ),
				'add_new_item'      => __( 'Add New Author', 'yith-plugin-book' ),
				'new_item_name'     => __( 'New Author Name', 'yith-plugin-book' ),
				'menu_name'         => __( 'Authors', 'yith-plugin-book' ),
			);
			$args1 = array(
				'hierarchical'      => false,
				'labels'            => $labels1,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'yith_tax_authors' ),
			);
			register_taxonomy( 'yith_pb_authors_tax', array( self::$post_type ), $args1 );

			$labels2 = array(
				'name'              => _x( 'Editorials', 'taxonomy editorials', 'yith-plugin-book' ),
				'singular_name'     => _x( 'Editorial', 'taxonomy aeditorialuthors', 'yith-plugin-book' ),
				'search_items'      => __( 'Search Editorials', 'yith-plugin-book' ),
				'all_items'         => __( 'All Editorials', 'yith-plugin-book' ),
				'parent_item'       => __( 'Parent Editorials', 'yith-plugin-book' ),
				'parent_item_colon' => __( 'Parent Editorials:', 'yith-plugin-book' ),
				'edit_item'         => __( 'Edit Editorial', 'yith-plugin-book' ),
				'update_item'       => __( 'Update Editorial', 'yith-plugin-book' ),
				'add_new_item'      => __( 'Add New Editorial', 'yith-plugin-book' ),
				'new_item_name'     => __( 'New Editorial Name', 'yith-plugin-book' ),
				'menu_name'         => __( 'Editorials', 'yith-plugin-book' ),
			);
			$args2 = array(
				'hierarchical'      => true,
				'labels'            => $labels2,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'yith_tax_editorials' ),
			);
			register_taxonomy( 'yith_pb_editorials_tax', array( self::$post_type ), $args2 );
		}
	}
}
