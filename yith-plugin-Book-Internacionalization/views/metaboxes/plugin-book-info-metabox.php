<?php

?>

<!-- Testimonial isbn field -->
<div>
    <label for="book_isbn"><?php _e( 'ISBN: ', 'yith-plugin-book' ); ?></label>
    <input type="text" name="_yith_pb_isbn" id = "book_isbn" value = "<?php echo esc_html( get_post_meta( $post->ID, '_yith_pb_isbn', true ) ); ?>">
</div>
<div>
    <label for="book_price"><?php _e( 'Price: ', 'yith-plugin-book' ); ?></label>
    <input type="number" name="_yith_pb_price" id = "book_price" value = "<?php echo esc_html( get_post_meta( $post->ID, '_yith_pb_price', true ) ); ?>">
</div>
<div>
    <label for="book_cap-type"><?php _e( 'Cap Type: ', 'yith-plugin-book' ); ?></label>
    <input type="text" name="_yith_pb_cap-type" id = "book_cap-type" value = "<?php echo esc_html( get_post_meta( $post->ID, '_yith_pb_cap-type', true ) ); ?>">
</div>
<div>
    <label for="book_language"><?php _e( 'Language: ', 'yith-plugin-book' ); ?></label>
    <input type="text" name="_yith_pb_language" id = "book_language" value = "<?php echo esc_html( get_post_meta( $post->ID, '_yith_pb_language', true ) ); ?>">
    
</div>

