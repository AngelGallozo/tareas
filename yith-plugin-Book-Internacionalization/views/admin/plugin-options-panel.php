<?php 

?>

<div class="wrap">
    <h1><?php esc_html_e('Settings', 'yith-plugin-book' );?></h1>

    <form method="post" action="options.php">
        <?php
            settings_fields( 'pb-options-page' );
		    do_settings_sections( 'pb-options-page' );
            submit_button();
        ?>

    </form>
</div>