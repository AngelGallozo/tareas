<?php
/*
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
    
    if ($hover_effect == 'zoom'){
        echo "<div class = 'yith-pt-testimonial zoom'>"; 
    }else{
        if ($hover_effect == 'highlight'){
            echo "<div class = 'yith-pt-testimonial highlight'>"; 
        }else{
            echo "<div class = 'yith-pt-testimonial'>"; 
        }
    }
?>  
    <div class = 'yith-pt-testimonial-user'>
        <?php if ( $show_image == 'yes' || $show_image == '') {
            echo  get_the_post_thumbnail( $post->ID );   
        } ?>
        <p class = 'yith-pt-testimonial-name'> <?php echo get_the_title( $post->ID );?> </p>
    </div>
        <p class = 'yith-pt-testimonial-message'> <?php echo $post->post_content; ?> </p>
<?php
    echo "</div>";
?>

    
