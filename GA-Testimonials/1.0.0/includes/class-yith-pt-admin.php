<?php
/*
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Admin' ) ) {
	
	/**
	 * YITH_PT_Admin
	 */
	class YITH_PT_Admin {
        /**
		 * Main Instance
		 *
		 * @var YITH_PT_Admin
		 * @since 1.0.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
         * @return YITH_PT_Admin Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }

		/**
		 * YITH_PT_Admin constructor.
		 */
		private function __construct() {
		}

	}	
}