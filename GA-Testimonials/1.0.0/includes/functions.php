<?php
/*
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * Include templates
 *
 * @param $file_name name of the file you want to include.
 * @param array $args (array) (optional) Arguments to retrieve.
 */
if ( ! function_exists( 'yith_pt_get_template' ) ) {
	function yith_pt_get_template( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_PT_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}


if ( ! function_exists( 'yith_pt_get_view' ) ) {
    /**
     * Include views
     * @param $file_name name of the file you want to include.
     * @param array $args (array) (optional) Arguments to retrieve
     * @return void
    */
    function yith_pt_get_view( $file_name, $args = array() ) {
        extract( $args );
        $full_path = YITH_PT_DIR_VIEWS_PATH . $file_name;
        if ( file_exists( $full_path ) ) {
            include $full_path;
        }
    }
}