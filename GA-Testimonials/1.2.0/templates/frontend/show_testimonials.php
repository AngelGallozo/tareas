<?php
/*
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
    
    $effect = '<div class ="yith-pt-tst '; // print testimonial div.
    // effect hover zoom.
    if ( $hover_effect === 'zoom'){
        $effect .= 'zoom';
    } else {
        // effect hover highlight.
        if ( $hover_effect === 'highlight' ){
            $effect .= 'highlight';
        }
    }
    // border vip.
    if ( get_post_meta( $post->ID, '_yith_pt_vip', true ) === 'yes' ) {
        $effect .= ' tst-vip';
    }
    $effect .= '" >';
    echo $effect;


?>  
    <!-- badge div-->
    <?php 
        if ( get_post_meta ( $post->ID, '_yith_pt_badge', true ) === 'yes' ) {
            echo '<div class="yith-pt-tst-badge"> <span style="background:' . get_post_meta ( $post->ID, '_yith_pt_badge-color', true ) . '">' . get_post_meta ( $post->ID, '_yith_pt_badge-text', true ) . '</span> </div>';
            //echo '<input id="yith-pt-tst-inp_color_badge" type="hidden" value="' . get_post_meta ( $post->ID, '_yith_pt_badge-color', true ) . '">';
        }
    ?>
    <!-- photo & data div-->
    <div class = 'yith-pt-tst-user'>
        <?php    
        if ( $show_image == 'yes' || $show_image == '') {
            echo  get_the_post_thumbnail( $post->ID );   
        } ?>
        <!-- user data div-->
        <div class='yith-pt-tst-data-user'>
            <p class = 'yith-pt-tst-name'> <?php echo get_the_title( $post->ID );?> </p>
            <?php 
            if ( get_post_meta( $post->ID, '_yith_pt_role', true ) ) {?>
                <p class= 'yith-pt-tst-role-company'>
                    <?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_role', true )) . __( ' at ', 'yith-plugin-testimonials' ) ;?> 
                    <a href="<?php  echo esc_html( get_post_meta( $post->ID, '_yith_pt_web-site', true ) );?> "> <?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_company', true ) );?> </a>
                </p>
            <?php }?>
            <p class='yith-pt-tst-email'> 
                <?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_email', true ));?>
            </p>
        </div>
    </div>
    <!-- user rating -->
    <div class='yith-pt-tst-rating'>
        <?php
            $rat = get_post_meta( $post->ID, '_yith_pt_rating', true );
                switch ( $rat ) {
                    case 1: 
                        echo '<span class="y-star"> &bigstar; </span> <span class="b-star"> &bigstar; </span> <span class="b-star"> &bigstar;</span> <span class="b-star"> &bigstar; </span><span class="b-star"> &bigstar;</span>';
                    break;
                    case 2: 
                        echo '<span class="y-star"> &bigstar; </span> <span class="y-star"> &bigstar; </span> <span class="b-star"> &bigstar; </span> <span class="b-star"> &bigstar; </span><span class="b-star"> &bigstar; </span>';
                    break;
                    case 3: 
                        echo '<span class="y-star"> &bigstar; </span> <span class="y-star"> &bigstar; </span> <span class="y-star"> &bigstar; </span> <span class="b-star">&bigstar; </span><span class="b-star"> &bigstar; </span>';
                    break;
                    case 4: 
                    echo '<span class="y-star"> &bigstar; </span> <span class="y-star"> &bigstar; </span> <span class="y-star"> &bigstar; </span> <span class="y-star"> &bigstar; </span><span class="b-star"> &bigstar; </span>';
                    break;
                    
                    case 5: 
                        echo '<span class="y-star"> &bigstar; </span> <span class="y-star"> &bigstar; </span> <span class="y-star"> &bigstar; </span> <span class="y-star"> &bigstar; </span><span class="y-star"> &bigstar; </span>';
                    break;

                    default:
                    echo '<span class="b-star"> &bigstar; </span> <span class="b-star"> &bigstar; </span> <span class="b-star"> &bigstar; </span> <span class="b-star"> &bigstar;</span><span class="b-star"> &bigstar; </span>';
                    break;
                }
        ?>
    </div>

    <!-- user message -->
    <p class = 'yith-pt-tst-message'> <?php echo $post->post_content; ?> </p>
<?php
echo "</div>"; //close testimonial div
?>
