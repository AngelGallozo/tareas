<?php 

?>

<div class="wrap">
    <h1><?php esc_html_e('Settings', 'yith-plugin-testimonial' );?></h1>

    <form method="post" action="options.php">
        <?php
            settings_fields( 'pt-options-page' );
		    do_settings_sections( 'pt-options-page' );
            submit_button();
        ?>

    </form>
</div>