<?php
/*
 * This file belongs to the YITH PB Plugin Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Shortcodes' ) ) {

	class YITH_PT_Shortcodes {

        /**
		 * Main Instance
		 *
		 * @var YITH_PT_Shortcodes
		 * @since 1.2.0
		 * @access private
		 */

		private static $instance;

		
        /**
         * Main plugin Instance
         *
         * @return YITH_PT_Post_Types Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PT_Post_Types constructor.
		 */
		private function __construct() {
			add_shortcode( 'yith_pt_show_testimonials', __CLASS__ . '::show_testimonials' );
		}

		//Display books and their content
        public static function show_testimonials( $atts ) {            
			//Enqueue custom CSS for the shortcode
			wp_enqueue_style('yith-pt-frontend-shortcode-css');
			$atts = shortcode_atts(
				array(
					'number' => '6',
					'ids' => '',
					'show_img' => 'yes',
					'hover_effect' => '',
				), 
				$atts
			);
			
			if ( $atts['number'] == '') {
				$atts['number'] = 6;
			}

			if ( $atts[ 'ids' ] == '' ) {
				$args = array(
					'numberposts'    => $atts['number'],
					'post_type'      => 'pt-testimonial',
					'publish_status' => 'published',
				);
			} else {
				$ids = explode( ',', $atts[ 'ids' ] );
				$args = array(
					'numberpost'    =>  count( $ids ),
					'post_type'      => 'pt-testimonial',
					'include'        => $ids,
					'publish_status' => 'published',
				 );

			}

			$posts = get_posts( $args );
			
			ob_start();

			foreach ( $posts as $post ) {
				yith_pt_get_template( '/frontend/show_testimonials.php', array(
					'post'			=> $post,
					'show_image'	=> $atts['show_img'],
					'hover_effect'	=> $atts['hover_effect'],  
				) );
			}

			return '<div class="yith-pt-tst-container">' . ob_get_clean() . '</div>';
        }
	}	
}