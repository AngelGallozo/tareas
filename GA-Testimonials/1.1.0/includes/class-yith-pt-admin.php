<?php
/*
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Admin' ) ) {
		
	/**
	 * YITH_PT_Admin
	 */
	class YITH_PT_Admin {
     	/**
	      * Main Instance
	      * @var YITH_PT_Admin
	      * @since 1.1.0
	      * @access private
	     */
		private static $instance;
		/**
		 * Main plugin Instance
         * @return YITH_PT_Admin Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }

		/**
		 * YITH_PT_Admin constructor.
		 */
		private function __construct() {
			add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'pt_ag_metabox_save') );
			
			// Add js for the shortcode.
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts') );
		}

		/**
		 * Setup the meta boxes
		 */
		public function add_meta_boxes() {
			add_meta_box( 'yith-pt-additional-information', __( 'Additional information', 'yith-plugin-pt' ), array(
			$this, 'view_meta_boxes' ), YITH_PT_Post_Types::$post_type );
		}


		
		/**
		 * Enqueue_scripts
		 *
		 * @return void
		 */
		public function enqueue_scripts() {
			wp_enqueue_script( 'yith-pt-admin-shortcode-js', YITH_PT_DIR_ASSETS_JS_URL . '/shortcode.js', array( 'jquery' ), YITH_PT_VERSION, true );
			wp_enqueue_style( 'wp-color-picker' );
    		wp_enqueue_script( 'my-script-handle', YITH_PT_DIR_ASSETS_JS_URL . '/shortcode.js', array( 'wp-color-picker' ), YITH_PT_VERSION, true );
		}

		/**
		 * View meta boxes
		 *
		 * @param $post
		 */
		public function view_meta_boxes( $post ) {
			yith_pt_get_view( '/metaboxes/plugin-testimonial-info-metabox.php', array( 'post' => $post) );
		}

		/**
		 * Save metabox values
		 * @param $post_id
		 */
		public function pt_ag_metabox_save( $post_id ) {
			// validate if user can edit the post.
			if ( ! current_user_can( 'edit_post' ) ) {
				return;
			}

			if ( YITH_PT_Post_Types::$post_type !== get_post_type( $post_id ) ) {
				return;
			}

			if ( isset( $_POST[ '_yith_pt_role' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_role', $_POST[ '_yith_pt_role' ] );
			}	

			if ( isset( $_POST[ '_yith_pt_company' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_company', $_POST[ '_yith_pt_company' ] );
			}
			
			if ( isset( $_POST[ '_yith_pt_email' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_email', $_POST[ '_yith_pt_email' ] );
			}

			if ( isset( $_POST[ '_yith_pt_web-site' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_web-site', $_POST[ '_yith_pt_web-site' ] );
			}
			if ( isset( $_POST[ '_yith_pt_rating' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_rating', $_POST[ '_yith_pt_rating' ] );
			}

			if ( isset( $_POST[ '_yith_pt_vip' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_vip', $_POST[ '_yith_pt_vip' ] );
			}

			
			if ( isset( $_POST[ '_yith_pt_badge' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_badge', $_POST[ '_yith_pt_badge' ] );
			}

			if( $_POST['_yith_pt_badge'] =='yes' ){
				if ( isset( $_POST[ '_yith_pt_badge-text' ] ) ) {
					update_post_meta( $post_id, '_yith_pt_badge-text', $_POST[ '_yith_pt_badge-text' ] );
				}

				if ( isset( $_POST[ '_yith_pt_badge-color' ] ) ) {
					update_post_meta( $post_id, '_yith_pt_badge-color', $_POST[ '_yith_pt_badge-color' ] );
				}

			}
		}
	}

}