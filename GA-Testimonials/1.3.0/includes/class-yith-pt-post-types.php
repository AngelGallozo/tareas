<?php
/*
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Post_Types' ) ) {

	class YITH_PT_Post_Types {

        /**
		 * Main Instance
		 *
		 * @var YITH_PT_Post_Types
		 * @since 1.3.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Post type name
		 *
		 * @var YITH_PT_Post_Types
		 * @since 1.3.0
		 * @access public
		 */
		public static $post_type = 'pt-testimonial';
		
		/**
         * Main plugin Instance
         *
         * @return YITH_PT_Post_Types Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PT_Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_type' ) );

		}

		/**
		 * Setup the 'Testimonial' custom post type
		 */
		public function setup_post_type() {
			$args = array(
				'label'        =>  __( 'Testimonial', 'yith-plugin-testimonials' ),
				'description'  =>  __( 'Testimonial post type', 'yith-plugin-testimonials'),
				'public'       => true,
				'menu_icon'    => 'dashicons-universal-access',
				'show_in_menu' => true,
				'show_ui'      => true,
				'rewrite'      => false,
				'supports'     => array( 'title', 'editor', 'author', 'thumbnail' ),
			);
			register_post_type( self::$post_type, $args );
		}
	}	
}