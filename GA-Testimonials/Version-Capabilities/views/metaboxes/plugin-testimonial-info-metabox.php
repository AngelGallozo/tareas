<?php

?>

<!-- Testimonial fields-->

<div>
    <label for="tst_role"><?php _e( 'Role: ', 'yith-plugin-testimonials' ); ?></label>
    <input type="text" name="_yith_pt_role" id = "tst_role" value = "<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_role', true ) ); ?>">
</div>
<div>
    <label for="tst_company"><?php _e( 'Company: ', 'yith-plugin-testimonials' ); ?></label>
    <input type="text" name="_yith_pt_company" id = "tst_company" value = "<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_company', true ) ); ?>">
</div>
<div>
    <label for="tst_web-site"><?php _e( 'Web Site Company: ', 'yith-plugin-testimonials' ); ?></label>
    <input type="url" name="_yith_pt_web-site" id = "tst_web-site" value = "<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_web-site', true ) ); ?>">
</div>
<div>
    <label for="tst_email"><?php _e( 'E-Mail: ', 'yith-plugin-testimonials' ); ?></label>
    <input type="email" name="_yith_pt_email" id = "tst_email" value = "<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_email', true ) ); ?>">
</div>
<div>
    <label for="tst_rating"><?php _e( 'Rating: ', 'yith-plugin-testimonials' ); ?></label>
    <input type="number" min="1" max="5" step="1" name="_yith_pt_rating" id = "tst_rating" value = "<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_rating', true ) ); ?>">
    
</div>
<div>
<label for= "tst_vip"> <?php _e( 'VIP: ', 'yith-plugin-testimonials' );?> </label>
    <label for="opt_yes"> <?php _e( 'Yes', 'yith-plugin-testimonials' );?> <input type="radio"  name="_yith_pt_vip" value='yes' id="tst_vip" <?php  checked( get_post_meta( $post->ID, '_yith_pt_vip', true ), 'yes' ); ?>> </label>
    <label for="opt_no"> <?php _e( 'No', 'yith-plugin-testimonials' );?>  <input type="radio"  name="_yith_pt_vip" value='no' id="tst_vip" <?php  checked( get_post_meta( $post->ID, '_yith_pt_vip', true ), 'no' ); ?>> </label>
</div>
<div>
<label for= "tst_badge"> <?php _e( 'Enable Badge: ', 'yith-plugin-testimonials' );?> </label>
    <label for="opt_yes"> <?php _e( 'Yes', 'yith-plugin-testimonials' );?> <input type="radio"  name="_yith_pt_badge" value='yes' id="tst_badge_y" <?php  checked( get_post_meta( $post->ID, '_yith_pt_badge', true ), 'yes' ); ?> checked> </label>
    <label for="opt_no"> <?php _e( 'No', 'yith-plugin-testimonials' );?>  <input type="radio"  name="_yith_pt_badge" value='no' id="tst_badge_n" <?php  checked( get_post_meta( $post->ID, '_yith_pt_badge', true ), 'no' ); ?>> </label>
</div>
<div class='yith-pt-tst-badge'>
    <label id='lb_badge_text' for="tst_badge-text"><?php _e( 'Badge Text: ', 'yith-plugin-testimonials' ); ?></label>
    <input type="text" name="_yith_pt_badge-text" id = "tst_badge-text" value = "<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_badge-text', true ) ); ?>">
    
</div>
<div class='yith-pt-tst-badge'>
    <label id='lb_badge_color' for="tst_badge-color"><?php _e( 'Badge Color: ', 'yith-plugin-testimonials' ); ?></label>
    <input type="text" name="_yith_pt_badge-color" value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_badge-color', true ) ); ?>" class="badge_color" data-default-color="#effeff" />    
</div>