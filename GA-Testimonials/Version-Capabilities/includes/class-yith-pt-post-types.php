<?php
/**
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Post_Types' ) ) {
	/**
	 * YITH_PT_Post_Types
	 */
	class YITH_PT_Post_Types {
		/**
		 * Main Instance
		 *
		 * @var YITH_PT_Post_Types
		 * @since 1.4.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Post type name
		 *
		 * @var YITH_PT_Post_Types
		 * @since 1.4.0
		 * @access public
		 */
		public static $post_type = 'pt-testimonial';
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PT_Post_Types Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PT_Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_type' ) );
			add_action( 'init', array( $this, 'register_taxonomy' ) );
		}

		/**
		 * Setup the 'Testimonial' custom post type
		 */
		public function setup_post_type() {
			$args = array(
				'label' 	      => __( 'Testimonial', 'yith-plugin-testimonials' ),
				'description'     => __( 'Testimonial post type', 'yith-plugin-testimonials' ),
				'public'          => true,
				'menu_icon'       => 'dashicons-universal-access',
				'show_in_menu'    => true,
				'show_ui'         => true,
				'rewrite'         => false,
				'supports'        => array( 'title', 'editor', 'author', 'thumbnail' ),
				'capability_type' => array( 'testimonial', 'testimonials' ),
				'map_meta_cap'    => false,
				'capabilities'    => array(
										'edit_post'			   => 'edit_testimonial', 
										'read_post'            => 'read_testimonial',
										'delete_post'          => 'delete_testimonial',
										'edit_posts'           => 'edit_testimonials',
										'edit_others_posts'    => 'edit_others_testimonials',
										'read_private_posts'   => 'read_private_testimonials',
										'publish_posts'        => 'publish_testimonials',
										'create_posts'   	   => 'create_testimonials',
										'edit_published_posts' => 'edit_published_testimonials',
 									),
			);
			register_post_type( self::$post_type, $args );
		}
		/**
		 * Registering taxonomy Students
		 *
		 * @return void
		 */
		public function register_taxonomy() {
			// Add Students taxonomy.
			$labels1 = array(
				'name'              => _x( 'Students', 'taxonomy students', 'yith-plugin-testimonials' ),
				'singular_name'     => _x( 'Student', 'taxonomy student', 'yith-plugin-testimonials' ),
				'search_items'      => __( 'Search Students', 'yith-plugin-testimonials' ),
				'all_items'         => __( 'All Students', 'yith-plugin-testimonials' ),
				'parent_item'       => __( 'Parent Students', 'yith-plugin-testimonials' ),
				'parent_item_colon' => __( 'Parent Students:', 'yith-plugin-testimonials' ),
				'edit_item'         => __( 'Edit Student', 'yith-plugin-testimonials' ),
				'update_item'       => __( 'Update Student', 'yith-plugin-testimonials' ),
				'add_new_item'      => __( 'Add New Student', 'yith-plugin-testimonials' ),
				'new_item_name'     => __( 'New Student Name', 'yith-plugin-testimonials' ),
				'menu_name'         => __( 'Estudents', 'yith-plugin-testimonials' ),
			);
			$args1 = array(
				'hierarchical'      => false,
				'labels'            => $labels1,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'yith_tax_students' ),
				'capabilities'      => array(
											'manage_terms' => 'manage_students',
											'edit_terms'   => 'edit_students',
											'delete_terms' => 'delete_estudents',
											'assign_terms' => 'assign_estudents',
				),
			);
			register_taxonomy( 'yith_pt_students_tax', array( self::$post_type ), $args1 );
		}
	}
}
