<?php //phpcs:ignore
/**
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_AJAX' ) ) {
	/**
	 * YITH_PT_AJAX
	 */
	class YITH_PT_AJAX {
		/**
		 * Main Instance
		 *
		 * @var YITH_PT_AJAX
		 * @since 1.4.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PT_AJAX Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PT_AJAX constructor.
		 */
		private function __construct() {
			add_action( 'wp_ajax_adm_votes', array( $this, 'adm_votes' ) );
		}	

		/**
		 * Function that updates the votes
		 *
		 * @return void
		 */
		public function adm_votes() {

			if ( isset( $_POST['post_id'] ) && isset( $_POST['click'] ) ) {
				$flag = true;
				// get id user. 
				$user_id = get_current_user_id();
				$post_id = $_POST['post_id'];
				// get users who already voted.
				$users = get_post_meta( $post_id, '_yith_pt_users-voted', true );
				// if not elements.
				if ( empty( $users ) ) {
					$users = [];
				} else {
					// search exists user in array.
					foreach ( $users as $user ) {
						if ( $user == $user_id ) {
							$flag = false;
						}
					}
				}

				if ( $flag ) { // Does not exist in the array.
					$click   = $_POST['click'];
					$votes   = get_post_meta( $post_id, '_yith_pt_votes', true );
					if ( '' === $votes ) {
							$votes = 0;
					}
					if ( 'up' == $click ) {
						update_post_meta( $post_id, '_yith_pt_votes', ++$votes );
					}
					if ( 'down' == $click ) {
						update_post_meta( $post_id, '_yith_pt_votes', --$votes );
					}

					array_push($users, $user_id );
					update_post_meta( $post_id, '_yith_pt_users-voted', $users );
					wp_send_json( array( 'message' => $votes . ' votes' ) );
				}
			}
		}
	}
}
