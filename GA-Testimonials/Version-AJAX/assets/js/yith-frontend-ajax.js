jQuery( function ( $ ) {
    $(".yith-user_vote").on('click', function(){  
        parent=$(this).parent();    
        post_id= $(parent).attr("data-post_id");    
        btn_votes=$(parent).children('span');
        click = $(this).attr("data-click");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: 'wp-admin/admin-ajax.php',
            data: {action: 'adm_votes', post_id:post_id, click:click
            },
            error: function(response) {
                console.log(response);
            },
            success: function(response) {
                $('.vote_counter-'+post_id).html(response['message']);
            }
        });     
    });
});