<?php
/**
 * This file belongs to the YITH PB Plugin Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Shortcodes' ) ) {
	/**
	 * YITH_PT_Shortcodes
	 */
	class YITH_PT_Shortcodes {

        /**
		 * Main Instance
		 *
		 * @var YITH_PT_Shortcodes
		 * @since 1.4.0
		 * @access private
		 */

		private static $instance;

		
        /**
         * Main plugin Instance
         *
         * @return YITH_PT_Post_Types Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PT_Post_Types constructor.
		 */
		private function __construct() {
			add_shortcode( 'yith_pt_show_testimonials', __CLASS__ . '::show_testimonials' );
			add_shortcode( 'yith_pt_last_3_testimonials', __CLASS__ . '::last_3_testimonials' );
		}

		//Display books and their content
        public static function show_testimonials( $atts ) {
			//Enqueue custom CSS for the shortcode.
			wp_enqueue_style( 'yith-pt-frontend-shortcode-css' );
			//set links color and border radius on testimonials.
			$l_color = get_option( '_yith_pt_def_links_color', '#0073aa' );
			$b_radius = get_option( '_yith_pt_def_border_radius', 7 );
        	$custom_css = "
                .yith-a-web-site{
					color: {$l_color};
                }  
				.yith-pt-tst{
					border-radius: {$b_radius}px;
				}
				";
        	wp_add_inline_style( 'yith-pt-frontend-shortcode-css', $custom_css );

			$atts = shortcode_atts(
				array(
					'student'=>'',
					'number' => get_option( '_yith_pt_def_show_number', 6 ),
					'ids' => '',
					'show_img' => get_option( '_yith_pt_def_show_image', 'yes' ),
					'hover_effect' => get_option( '_yith_pt_def_hover_effect', 'none' ),
				), 
				$atts
			);
			
			if (  '' === $atts['show_img'] ) {
				$atts['show_img'] = get_option( '_yith_pt_def_show_image', 'yes' );
			}

			if ( '' === $atts['hover_effect'] ) {
				$atts['hover_effect'] = get_option( '_yith_pt_def_hover_effect', 'none' );
			}

			if ( '' === $atts['number'] ) {
				$atts['number'] = get_option( '_yith_pt_def_show_number', 6 );
				if ( $atts['number'] == 0 ) {
					$atts['number'] = 6;
				}
			}

			if ( '' === $atts[ 'ids' ] ) {
				$args = array(
					'numberposts'    => $atts['number'],
					'post_type'      => 'pt-testimonial',
					'publish_status' => 'published',
				);
			} else {
				$ids = explode( ',', $atts['ids'] );
				$args = array(
					'numberpost'     =>  count( $ids ),
					'post_type'      => 'pt-testimonial',
					'include'        => $ids,
					'publish_status' => 'published',
				 );

			}
			// In case the taxonomy is needed.
			if ( '' !== $atts['student'] ) {
				$terms = get_terms( array( 
					'taxonomy' => 'yith_pt_students_tax',
					'name'   => $atts['student'],
				) );
				$tax = ['tax_query' => array(
												array(
														'taxonomy' => 'yith_pt_students_tax',
														'field'    => 'term_id',
														'terms'    => $terms[0]->term_id,
													)
											)];
				$args = $args + $tax;
			}

			$posts = get_posts( $args );
			ob_start();

			foreach ( $posts as $post ) {
				yith_pt_get_template( '/frontend/show_testimonials.php', array(
					'post'			=> $post,
					'show_image'	=> $atts['show_img'],
					'hover_effect'	=> $atts['hover_effect'],
				) );
			}

			return '<div class="yith-pt-tst-container">' . ob_get_clean() . '</div>';
		}

		
		/**
		 * Return last 3 testimonials
		 *
		 * @return $pt_transient -> html containing the 3 posts
		 */
		public static function last_3_testimonials() {
			//Enqueue custom CSS for the shortcode.
			wp_enqueue_style( 'yith-pt-frontend-shortcode-css' );
			//set links color and border radius on testimonials.
			$l_color = get_option( '_yith_pt_def_links_color', '#0073aa' );
			$b_radius = get_option( '_yith_pt_def_border_radius', 7 );
        	$custom_css = "
                .yith-a-web-site{
					color: {$l_color};
                }  
				.yith-pt-tst{
					border-radius: {$b_radius}px;
				}
				";
        	wp_add_inline_style( 'yith-pt-frontend-shortcode-css', $custom_css );

			$pt_transient = get_transient( 'yith-pt-last-3-testimonials-transient' );
			if ( ! $pt_transient ) {
				$args = array(
					'numberposts'    => 3,
					'post_type'      => 'pt-testimonial',
					'publish_status' => 'published',
				);
				$posts = get_posts( $args );
				ob_start();
				error_log( print_r( $pt_transient,true ) );
				foreach ( $posts as $post ) {
					yith_pt_get_template( '/frontend/show_testimonials.php', array(
						'post'			=> $post,
						'show_image'	=> get_option( '_yith_pt_def_show_image', 'yes' ),
						'hover_effect'	=> get_option( '_yith_pt_def_hover_effect', 'none' ),
					) );
				}
				$pt_transient = '<div class="yith-pt-tst-container">' . ob_get_clean() . '</div>';
				set_transient( 'yith-pt-last-3-testimonials-transient', $pt_transient, 2 * HOURS_IN_SECONDS );
			}
			return $pt_transient;
		}
	}	
}