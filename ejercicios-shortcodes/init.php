<?php 
/**
 * Plugin Name:       Ejercicio-shortcodes
 * Plugin URI:        https://example.com/plugins/the-basics/
 * Description:       Handle the basics with this plugin.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Angel Gallozo
 * Author URI:        https://author.example.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       yith-shortcodes
 * Domain Path:       /languages
 * */

/**
 * get_post_shcode
 *
 * @param  mixed $atts post atributes 
 * @param  mixed $content
 * @return $p post content
 */
function get_post_shcode( $atts, $content = null ) {
	$pst = null;
	$p = null;
	$atts = shortcode_atts( array( 'post_id' => 0 ), $atts );
	if ( $atts['post_id'] > 0 ) {
		$pst = get_post( $atts['post_id'] );
		$p = $pst->post_content;
	} else {
		$p = '<p>Post Sin Contenido</p>';
    }
    return $p;
}
add_shortcode( 'shortcode1', 'get_post_shcode' );



/**
 * Show 5 last post shcode
 *
 * @param  mixed $atts
 * @param  mixed $content
 * @return $result posts list
 */
function show_5_post( $atts, $content = null ) {
	$ps = get_posts(); // get all posts.
	$result = '<ul>'; // the post will be saved in a unordered list.
	for ( $i = 0; $i < 5; $i++ ) {
    	$result .= '<li> <a href="' . $ps[$i]->guid . '">' . $ps[$i]->post_title .'</a> </li>';
	}
	$result .= '</ul>';
	return $result;
}
add_shortcode( 'shortcode2', 'show_5_post' );


/**
 * Show user data 
 *
 * @param  mixed $atts
 * @param  mixed $content
 * @return void
 */
function show_udata( $atts, $content = null ) {
	$result = '';
	$atts = shortcode_atts( array('user_id' => 0 ), $atts );
	if ( $atts['user_id'] > 0 ) {
		$user = get_user_by( 'id', $atts['user_id'] );
		$result = '<p>Nombre y Apellido: ' . $user->display_name . ' Nickname: ' . $user->user_nicename . '</p>';
    } else {
		$result = '<p>Usuario Inexistente</p>';
	}
	return $result;
}
add_shortcode( 'shortcode3', 'show_user_data_shcode' );




