<?php // phpcs:ignore
/**
 * Plugin Name: YITH Plugin Raffle
 * Description: Rafle for YITH Plugins
 * Version: 1.0.0
 * Author: Angel Gallozo
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-raffle
 */

! defined( 'ABSPATH' ) && exit;  // check if defined ABSPATH.

/* where defined PATH for Style, Assets, Templates, Views */
if ( ! defined( 'YITH_PR_VERSION' ) ) {
	define( 'YITH_PR_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PR_DIR_URL' ) ) {
	define( 'YITH_PR_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PR_DIR_PATH' ) ) {
	define( 'YITH_PR_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PR_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PR_DIR_ASSETS_URL', YITH_PR_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PR_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PR_DIR_ASSETS_CSS_URL', YITH_PR_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PR_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PR_DIR_ASSETS_JS_URL', YITH_PR_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PR_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PR_DIR_INCLUDES_PATH', YITH_PR_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PR_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PR_DIR_TEMPLATES_PATH', YITH_PR_DIR_PATH . '/templates' );
}


if ( ! defined( 'YITH_PR_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PR_DIR_VIEWS_PATH', YITH_PR_DIR_PATH . 'views' );
}

// Different way to declare a constant.

! defined( 'YITH_PR_INIT' ) && define( 'YITH_PR_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_PR_SLUG' ) && define( 'YITH_PR_SLUG', 'yith-plugin-raffle' );
! defined( 'YITH_PR_SECRETKEY' ) && define( 'YITH_PR_SECRETKEY', 'zd9egFgFdF1D8Azh2ifA' );


if ( ! function_exists( 'yith_pr_init_classes' ) ) {
	/**
	 * Include the scripts
	 *
	 * @return void
	 */
	function yith_pr_init_classes() {

		load_plugin_textdomain( 'yith-plugin-raffle', false, basename( dirname( __FILE__ ) ) . '/languages/' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_PR_DIR_INCLUDES_PATH . '/class-yith-pr-plugin-raffle.php';

		if ( class_exists( 'YITH_PR_Plugin_Raffle' ) ) {
			/*
			*	Call the main function
			*/
			yith_pr_plugin_raffle();
		}
	}
}

add_action( 'plugins_loaded', 'yith_pr_init_classes', 11 );

if ( ! function_exists( 'yith_raffle_install' ) ) {
    function yith_raffle_install() {
        do_action( 'yith_raffle_init');
        YITH_RAFFLE_DB::install();  // This is the class where you can create the new datatable
    }
 
    add_action( 'plugins_loaded', 'yith_raffle_install', 11 );
 }