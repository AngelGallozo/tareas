��    	      d      �       �   8   �   8     "   S     v     �     �  ;   �  )   �  �  �  0   �  ,   �  '   �     	       	     <   (  $   e                               	               Congratulations, you have entered the raffle. Good Luck! Error, I do not mark that it participated in the raffle. Error, Invalid or Incomplete Data. Last Name:  Name:  REGISTER This user has already been registered in the raffle before. Yes, I want to participate in the raffle. Project-Id-Version: YITH Plugin Raffle 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/yith-sorteo
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2021-04-05 09:44-0300
X-Generator: Poedit 2.4.2
X-Domain: yith-plugin-raffle
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: es_AR
 Felicidades, has entrado en el sorteo. ¡Suerte! Error, no marcó que participa en el sorteo. Error, datos NO válidos o incompletos. Apellidos:  Nombre:  Registrar Este usuario ya ha sido inscrito en el sorteo anteriormente. Sí, quiero participar en el sorteo. 