<?php //phpcs:ignore
/**
 * This file belongs to the YITH PR Raffle.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PR_AJAX' ) ) {
	/**
	 * YITH_PR_AJAX
	 */
	class YITH_PR_AJAX {
		/**
		 * Main Instance
		 *
		 * @var YITH_PR_AJAX
		 * @since 1.0.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PR_AJAX Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PR_AJAX constructor.
		 */
		private function __construct() {
			add_action( 'wp_ajax_raffle_register', array( $this, 'raffle_register' ) );
			add_action( 'wp_ajax_nopriv_raffle_register', array( $this, 'raffle_register' ) );
		}	
		/**
		 * Function that updates the votes
		 *
		 * @return void
		 */
		public function raffle_register() {
			$valid_data = true;
			$user_data = $_POST['user_data'];
			$mensaje = '';
			if ( 'no' === $_POST['check'] ) {
				$mensaje =  __( 'Error, I do not mark that it participated in the raffle.', 'yith-plugin-raffle' );
				$color   = 'red';

			} else {
				if ( isset( $_POST['user_data'] ) ) { // If it has contains data, user not logged in. 
					if ( '' === $user_data['name'] || '' === $user_data['last_name'] || ! filter_var( $user_data['email'], FILTER_VALIDATE_EMAIL ) ) { 
						$mensaje = __( 'Error, Invalid or Incomplete Data.', 'yith-plugin-raffle' );
						$color   = 'red';
						$valid_data = false;

					} else {
						$name    = $user_data['name'];
						$l_name  = $user_data['last_name'];
						$email   = $user_data['email'];
					}
				} else { //if it does not contain data, User logged in
					$user    = wp_get_current_user();
					$name    = $user->user_nicename;
					$l_name  = $user->display_name;
					$email   = $user->user_email;
				}

				if ( $valid_data ) {
					global $wpdb;
					$user_regist = $wpdb->get_results( "SELECT email FROM wp_yith_raffle" );
					$registered = false;
					$index = 0;
					$max = count( $user_regist );

					// search user in table.
					while ( ! $registered && $index < $max ) {
						if ( $email === $user_regist[ $index ]->email ) {
							$registered = true;
						}
						++$index;
					}

					// validate if the user is registered.
					if ( ! $registered ) {
						// insert in DB.
						$table_name = $wpdb->prefix . 'yith_raffle';
						$wpdb->insert( $table_name, //phpcs:ignore
									   array(
										   		'nombre'    => $name,
												'apellidos' => $l_name,
												'email'     => $email,
											)
						);
						$mensaje = __( 'Congratulations, you have entered the raffle. Good Luck!', 'yith-plugin-raffle' );
						$color   = 'green';
					} else {
						$mensaje = __( 'This user has already been registered in the raffle before.', 'yith-plugin-raffle' );
						$color   = 'red';
					}
				}
			}
			wp_send_json( array( 
								'message' => $mensaje, 
								'color_msg' => $color, 
                		  )
			);
		}
	}
}

