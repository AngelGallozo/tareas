<?php

/**
 * Plugin Name: YITH Plugin GA Testimonials
 * Description: GA Testimonials for YITH Plugins
 * Version: 1.3.0
 * Author: Angel Gallozo
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-testimonials
 */

! defined( 'ABSPATH' ) && exit;  // check if defined ABSPATH.

/* where defined PATH for Style, Assets, Templates, Views */
if ( ! defined( 'YITH_PT_VERSION' ) ) {
	define( 'YITH_PT_VERSION', '1.4.0' );
}

if ( ! defined( 'YITH_PT_DIR_URL' ) ) {
	define( 'YITH_PT_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PT_DIR_PATH' ) ) {
	define( 'YITH_PT_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PT_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PT_DIR_ASSETS_URL', YITH_PT_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PT_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PT_DIR_ASSETS_CSS_URL', YITH_PT_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PT_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PT_DIR_ASSETS_JS_URL', YITH_PT_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PT_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PT_DIR_INCLUDES_PATH', YITH_PT_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PT_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PT_DIR_TEMPLATES_PATH', YITH_PT_DIR_PATH . '/templates' );
}


if ( ! defined( 'YITH_PT_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PT_DIR_VIEWS_PATH', YITH_PT_DIR_PATH . 'views' );
}

// Different way to declare a constant.

! defined( 'YITH_PT_INIT' ) && define( 'YITH_PT_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_PT_SLUG' ) && define( 'YITH_PT_SLUG', 'yith-plugin-testimonials' );
! defined( 'YITH_PT_SECRETKEY' ) && define( 'YITH_PT_SECRETKEY', 'zd9egFgFdF1D8Azh2ifA' );


if ( ! function_exists( 'yith_pt_init_classes' ) ) {
	/**
	 * Include the scripts
	 *
	 * @return void
	 */
	function yith_pt_init_classes() {

		load_plugin_textdomain( 'yith-plugin-testimonials', false, basename( dirname( __FILE__ ) ) . '/languages/' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_PT_DIR_INCLUDES_PATH . '/class-yith-pt-plugin-testimonials.php';

		if ( class_exists( 'YITH_PT_Plugin_Testimonial' ) ) {
			/*
			*	Call the main function
			*/
			yith_pt_plugin_testimonial();
		}
	}
}

add_action( 'plugins_loaded', 'yith_pt_init_classes', 11 );
