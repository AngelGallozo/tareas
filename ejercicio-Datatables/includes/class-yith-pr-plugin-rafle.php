<?php
/**
 * This file belongs to the YITH PR Plugin Rafle.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PR_Plugin_Rafle' ) ) {	
	/**
	 * YITH_PR_Plugin_Testimonial
	 */
	class YITH_PR_Plugin_Rafle {
        /**
		 * Main Instance
		 *
		 * @var YITH_PR_Plugin_Rafle
		 * @since 1.0.0
		 * @access private
		 */

		private static $instance;
        /**
		 * Main Admin Instance
		 *
		 * @var YITH_PR_Plugin_Rafle_Admin
		 * @since 1.0.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_PR_Plugin_Rafle_Frontend
		 * @since 1.0.0
		 */
        public $frontend = null;

		/**
		 * Main Shortcodes Instance
		 * @var YITH_PR_Plugin_Rafle_Shortcodes
		 * @since 1.0.0
		 */
        public $shortcodes = null;
        /**
         * Main plugin Instance
         *
         * @return YITH_PR_Plugin_Rafle Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PR_Plugin_Rafle constructor.
		 */
		private function __construct() {
            $require = apply_filters( 'yith_pr_require_class',
				 array(
					'common'   => array(
						'includes/functions.php',
						'includes/class-yith-pr-shortcodes.php',
						'includes/class-yith-pr-ajax.php',
						'includes/class-yith-pr-db.php',
					),
					'admin' => array(
						'includes/class-yith-pr-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-pr-frontend.php',
					),
				)
			);
			$this->_require($require);
			$this->init_classes();
			// Finally call the init function
			$this->init();
		}
	
		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param $main_classes array The require classes file path
		 *
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 * @since  1.0.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require($main_classes)
		{
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && !is_admin() || (defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_PR_DIR_PATH . $class ) ) {
						require_once( YITH_PR_DIR_PATH . $class );
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 * @since  1.0.0
		 **/
		public function init_classes(){
			$this->shortcodes = YITH_PR_Shortcodes::get_instance();
			YITH_PR_AJAX::get_instance();
		}

		/**
         * Function init()
         *
         * Instance the admin or frontend classes
         *
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         * @since  1.0.0
         * @return void
         * @access protected
         */
        public function init()
        {
            if ( is_admin() ) {
                $this->admin =  YITH_PR_Admin::get_instance();
            }

            if ( !is_admin() || (defined('DOING_AJAX') && DOING_AJAX)) {
                $this->frontend = YITH_PR_Frontend::get_instance();
            }
        }

	}	
}
/**
 * Get the YITH_PR_Plugin_Rafle instance
 *
 * @return YITH_PR_Plugin_Rafle
 */
if ( ! function_exists( 'yith_pr_plugin_rafle' ) ) {
	function yith_pr_plugin_rafle() {
		return YITH_PR_Plugin_rafle::get_instance();
	}
}