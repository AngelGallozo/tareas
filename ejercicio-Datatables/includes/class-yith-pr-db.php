<?php
/**
 * This file belongs to the YITH PR Plugin RafFle
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
   exit;
} // Exit if accessed directly

if ( !class_exists( 'YITH_RAFFLE_DB' ) ) {
   /**
    * YITH RAFLE Database
    *
     * @since 1.0.0
    */
   class YITH_RAFFLE_DB {
        /**
         * DB version
         *
         * @var string
         */
        public static $version = '1.0.0';
        /**
         * Datatable
         *
         * @var string
         */
        public static $raffle_table = 'yith_raffle';
        /**
         * Constructor
         *
         * @return YITH_RAFFLE_DB
         */
        private function __construct() {
        }
        
        /**
         * install
         *
         * @return void
         */
        public static function install() {
            self::create_db_table();
        }

        /**
         * Create table for Notes
         *
         * @param bool $force
         */
        public static function create_db_table( $force = false ) {
            global $wpdb;
            
            $current_version = get_option( 'yith_raffle_db_version' );

            if ( $force || $current_version != self::$version ) {
                $wpdb->hide_errors();

                $table_name      = $wpdb->prefix . self::$raffle_table; // The table always must have a prefix.
                $charset_collate = $wpdb->get_charset_collate();

                $sql
                    = "CREATE TABLE $table_name (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `nombre` varchar(30) NOT NULL,
                    `apellidos` varchar(50) NOT NULL,
                    `email` varchar(50) NOT NULL,
                    PRIMARY KEY (id)
                    ) $charset_collate;";

                if ( ! function_exists( 'dbDelta' ) ) {
                    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
                }
                dbDelta( $sql ); // Use DBdelta function for create the table.
                update_option( 'yith_pr_db_version', self::$version );
            }
        }

    }
}
