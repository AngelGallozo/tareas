<?php //phpcs:ignore
/**
 * This file belongs to the YITH PR Plugin Raffle.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PR_Shortcodes' ) ) {
	/**
	 * YITH_PR_Shortcodes
	 */
	class YITH_PR_Shortcodes {

        /**
		 * Main Instance
		 *
		 * @var YITH_PR_Shortcodes
		 * @since 1.0.0
		 * @access private
		 */

		private static $instance;
        /**
         * Main plugin Instance
         *
         * @return YITH_PR_Shortcodes Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
		/**
		 * YITH_PR_Shortocdes constructor.
		 */
		private function __construct() {
			add_shortcode( 'yith_pr_show_raffle', __CLASS__ . '::show_raffle' );
		}
		/**
		 * Show Rafle Form 
		 * @return void
		 */
		public static function show_raffle() {
			if ( ! is_user_logged_in() ) {
				?>
				<div>
					<label for="lb_name"> <?php _e( 'Name: ', 'yith-plugin-raffle' ); ?></label>
					<input type="text" name="_yith_pr_name" id = "raffle_name">
				</div>
				<div>
					<label for="lb_last-name"><?php _e( 'Last Name: ', 'yith-plugin-raffle' ); ?> </label>
					<input type="text" name="_yith_pr_last-name" id = "raffle_last-name">
				</div>
				<div>
					<label for="tst_email"><?php _e( 'E-Mail: ', 'yith-plugin-raffle' ); ?></label>
					<input type="email" name="_yith_pr_email" id = "raffle_email">
				</div>
			<?php
			}
			?>
			<div>
				<label for="lb_checkbox">
					<input type="checkbox" class="pr-accept-raffle" name="_yith_pr_accept_raffle" id = "raffle_check">
					<?php  _e( 'Yes, I want to participate in the raffle.', 'yith-plugin-raffle' ); ?>
				</label>
			</div>
			<div>
				<button id= 'yith-pr-btn-register'> <?php _e('REGISTER' , 'yith-plugin-raffle' ); ?></button>
				<p id='raffle-msg' ></p>
			</div>
		<?php
		}
	}
}