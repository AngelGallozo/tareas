<?php //phpcs:ignore
/**
 * This file belongs to the YITH PR Plugin Raffle.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PR_Frontend' ) ) {
	/**
	 * YITH_PR_Frontend
	 */
	class YITH_PR_Frontend {
		/**
		 * Main Instance
		 *
		 * @var YITH_PR_Frontend
		 * @since 1.1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Main plugin Instance
		 * @return YITH_PR_Frontend Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PR_Frotend constructor.
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		}
		/**
		 * Enqueue_scripts Function
		 *
		 * @return void
		 */
		public function enqueue_scripts() {
			wp_register_script( 'yith-pt-frontend-shortcode-js', YITH_PR_DIR_ASSETS_JS_URL . '/main.js', array( 'jquery' ), YITH_PR_VERSION );
			wp_enqueue_script( 'yith-pt-frontend-shortcode-js' );
		}
	}
}
