jQuery( function ( $ ) {
    
    $("#yith-pr-btn-register").on('click', function(){  
        user_data={
                    name: $('#raffle_name').val(),
                    last_name: $('#raffle_last-name').val(),
                    email: $('#raffle_email').val(),
        }

        check= ($('#raffle_check').prop('checked')) ? 'yes' : 'no';
        $.ajax({
            type: "POST",
            dataType: "json",
            url: 'wp-admin/admin-ajax.php',
            data: {action: 'raffle_register', user_data:user_data ,check:check },
            error: function(response) {
                console.log(response);
            },
            success: function(response) {
                $('#raffle-msg').text(response['message']);
                $('#raffle-msg').css('color',response['color_msg']);
            }
        });     
    });
});