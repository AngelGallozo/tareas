<?php 
/**
 * Plugin Name:       Ejercicio-Enqueue-Scripts
 * Plugin URI:        https://example.com/plugins/the-basics/
 * Description:       Handle the basics with this plugin.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Angel Gallozo
 * Author URI:        https://author.example.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       yith-enqueue-scripts
 * Domain Path:       /languages
 * */
