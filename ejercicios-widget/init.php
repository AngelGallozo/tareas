<?php 
/**
 * Plugin Name:       Ejercicio-Widget  
 * Plugin URI:        https://example.com/plugins/the-basics/
 * Description:       Handle the basics with this plugin.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Angel Gallozo
 * Author URI:        https://author.example.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       yith-widget
 * Domain Path:       /languages
 * */


/**
* Excercise 1 Widget: Show form, a class was created to make the widget
*/
add_action( 'widgets_init', function() {
    register_widget( 'FormWidget' );
  } );

  class FormWidget extends WP_Widget {
    // class constructor
    public function __construct() {
        $widget_ops = array(
            'classname' => 'formwidget',
            'description' => 'Primer Widget',
        );
        parent::__construct( 'yith-formWidget', 'Yith-Formulario', $widget_ops );
    }
    
    // output the widget content on the front-end.
    public function widget( $args, $instance )
    {
        ?>   
        <label for = ""> Nombre </label>
        <input type = "text">
        <label for = "" > Apellido </label>
        <input type="text">
        <label for=""> Dirección</label>
        <input type="text">
        <label for="">Telefono</label>
        <input type="text">
    <?php
}
    
    // output the option form field in admin Widgets screen
    public function form( $instance ) {
     ?>   
        <label for=""> Nombre</label>
        <input type="text">
        <label for=""> Apellido</label>
        <input type="text">
        <label for=""> Dirección</label>
        <input type="text">
        <label for="">Telefono</label>
        <input type="text">
    <?php

    }
    
    // save options
    public function update( $new_instance, $old_instance ) {}
    }


/**
* Excercise 2 Widget: Show last 5 posts, a class was created to make the widget
*/

add_action( 'widgets_init', function() {
    register_widget( 'L5PostWidget' );
  } );  
  
  class L5PostWidget extends WP_Widget {
    public function __construct() {
        $widget_ops = array(
            'classname' => 'l5postwidget',
            'description' => 'Segundo Widget'
        );
        parent::__construct('yith-l5postsWidget', 'Yith-Ultimos 5 Posts', $widget_ops);
    }

    // output the widget content on the front-end.
    public function widget( $args, $instance ) {
        $ps = get_posts(); // get all posts.
		?>
        <p> Últimos 5 Post Creados </p>
        <?php
        for( $i = 0 ; $i < 5; $i++ ) {
            echo( '<li> <a href="' . $ps[$i]->guid . '">' . $ps[$i]->post_title .'</a> </li>' );
        }
    }

    /*/ // output the option form field in admin Widgets screen.
    public function form( $instance ) {
        ?>
        <p>Últimos 5 Post Creados</p>
        <ul>
            <li> <a href="#"> Post x</a></li>
            <li> <a href="#"> Post x</a></li>
            <li> <a href="#"> Post x</a></li>
            <li> <a href="#"> Post x</a></li>
            
        </ul>
        <?php    
    }*/

    public function form( $instance ) {
        $ps = get_posts(); // get all posts.
		?>
        <p> Últimos 5 Post Creados </p>
        <?php
        for ( $i = 0; $i < 5; $i++ ) {
            echo( '<li> <a href="' . $ps[$i]->guid . '">' . $ps[$i]->post_title . '</a> </li>' );
        }
    }

    // save options
    public function update( $new_instance, $old_instance ) {}
}