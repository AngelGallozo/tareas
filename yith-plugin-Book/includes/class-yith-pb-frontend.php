<?php
/*
 * This file belongs to the YITH PB Plugin Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PB_Frontend' ) ) {

	class YITH_PB_Frontend {
        /**
		 * Main Instance
		 *
		 * @var YITH_PB_Frontend
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
        
        /**
         * Main plugin Instance
         *
         * @return YITH_PB_Frontend Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PB_Frotend constructor.
		 */
		private function __construct() {
			add_filter( 'the_content', array( $this, 'add_additional_info') );
		}


		/**
		 * Add ISBN, Price, Capt-Type, Language
		 *
		 * @param mixed $content
		 * @return $content
		 */
		public function add_additional_info( $content ) {
			global $post;
			if ( $post ) {
				$content .= '<br> 
				<p> ISBN: ' . 
					get_post_meta( $post->ID, '_yith_pb_isbn', true ) .
					'<br>  Precio: ' .
					get_post_meta( $post->ID, '_yith_pb_price', true ) .
					'<br> Tipo de Tapa: ' .
					get_post_meta( $post->ID, '_yith_pb_cap-type', true ) .
					'<br>  Idioma: ' .
					get_post_meta( $post->ID, '_yith_pb_language', true ) . 
				'</p>';
				return $content;
			}
		}
	}	
}