<?php
/*
 * This file belongs to the YITH PB Plugin Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PB_Admin' ) ) {

	class YITH_PB_Admin {

        /**
		 * Main Instance
		 *
		 * @var YITH_PB_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
         * @return YITH_PB_Admin Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }

		/**
		 * YITH_PB_Admin constructor.
		 */
		private function __construct() {
			add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'pb_ag_metabox_save') );
		}

		/**
		 * Setup the meta boxes
		 */
		public function add_meta_boxes() {
			add_meta_box( 'yith-ps-additional-information', __( 'Additional information', 'yith-plugin-pb' ), array(
			$this, 'view_meta_boxes' ), YITH_PB_Post_Types::$post_type );
		}

		/**
		 * View meta boxes
		 *
		 * @param $post
		 */
		public function view_meta_boxes( $post ) {
			yith_pb_get_view( '/metaboxes/plugin-book-info-metabox.php', array( 'post' => $post) );
		}

		/**
		 * Save metabox values
		 * @param $post_id
		 */
		public function pb_ag_metabox_save( $post_id ) {
			// validate if user can edit the post
			if ( ! current_user_can( 'edit_post' ) ) {
				return;
			}

			if ( YITH_PB_Post_Types::$post_type !== get_post_type( $post_id ) ) {
				return;
			}

			if ( isset( $_POST[ '_yith_pb_isbn' ] ) ) {
				update_post_meta( $post_id, '_yith_pb_isbn', $_POST[ '_yith_pb_isbn' ] );
			}	

			if ( isset( $_POST[ '_yith_pb_price' ] ) ) {
				update_post_meta( $post_id, '_yith_pb_price', $_POST[ '_yith_pb_price' ] );
			}
			if ( isset( $_POST[ '_yith_pb_cap-type' ] ) ) {
				update_post_meta( $post_id, '_yith_pb_cap-type', $_POST[ '_yith_pb_cap-type' ] );
			}
			if ( isset( $_POST[ '_yith_pb_language' ] ) ) {
				update_post_meta( $post_id, '_yith_pb_language', $_POST[ '_yith_pb_language' ] );
			}
		}
	}	
}